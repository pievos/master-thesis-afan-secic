"""
    Graph Neural Network explanation based on Higher-Order Explanations of Graph Neural
    Networks via Relevant Walks <https://arxiv.org/pdf/2006.03589.pdf>

    :author: Afan Secic (main developer), Anna Saranti (corrections and refactoring)
    :copyright: © 2020 HCI-KDD (ex-AI) group
    :date: 2020-12-01
"""
import utils
import numpy as np
import scipy
import networkx as nx
import dgl
import dgl.data
import torch.nn.functional as F
import torch
from torch_geometric.data import DataLoader
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import copy, time

from features_computation import get_genes, get_genes_bernoulli
from gnn import Classifier
from gnn_lrp import Classifier_LRP
from gnn_training_utils import collate
from graph_dataset import GraphDataset

from torch_geometric.nn.models import GNNExplainer

from torch_geometric.datasets import TUDataset
from torch_geometric.data.data import Data


import matplotlib.pyplot as plt
import pickle
import torch.nn as nn
from torch_geometric.nn import GCNConv, global_add_pool, GINConv

from torch.nn import Sequential, Linear, ReLU
from dataset import generate_lrp
from lrp_explain import explain

########################################################################################################################
# [1.] Initialize all graphs ::: Structure, features and computed labels ===============================================
########################################################################################################################
graphs_nr = 1000
nodes_per_graph_nr = 7

dataset = generate_lrp(graphs_nr,nodes_per_graph_nr)

########################################################################################################################
# [2.] GNN training preparation: define the dataset of the graphs and the batch details ================================
########################################################################################################################
dataloader = DataLoader(
    dataset,
    batch_size=64,
    shuffle=True)


########################################################################################################################
# [3.] GNN training ====================================================================================================
########################################################################################################################
epoch_nr = 15
input_dim = 7
n_classes = 2
model = Classifier_LRP(input_dim, n_classes)
model.train()
model.double()
opt = torch.optim.Adam(model.parameters(), lr = 0.001)
for epoch in range(epoch_nr):
    epoch_loss = 0
    graph_idx = 0
    for data in dataloader:
        batch = []
        for i in range(data.y.size(0)):
            for j in range(nodes_per_graph_nr):
                batch.append(i)
        logits = model(data, torch.tensor(batch))
        loss = F.nll_loss(logits, data.y)

        opt.zero_grad()
        loss.backward()
        opt.step()

        epoch_loss += loss.detach().item()
        graph_idx += 1
    epoch_loss /= graph_idx

    print('Epoch {}, loss {:.4f}'.format(epoch, epoch_loss))

########################################################################################################################
# [4.] Prediction ======================================================================================================
########################################################################################################################

true_class_array = []
predicted_class_array = []
model.eval()
correct = 0
true_class_array = []
predicted_class_array = []

for data in dataloader:
    batch = []
    for i in range(data.y.size(0)):
        for j in range(nodes_per_graph_nr):
            batch.append(i)

    output = model(data, torch.tensor(batch))

    predicted_class = output.max(dim=1)[1]
    true_class = data.y
    predicted_class_array = np.append(predicted_class_array, predicted_class)
    true_class_array = np.append(true_class_array, true_class)

    correct += predicted_class.eq(data.y).sum().item()

confusion_matrix_gnn = confusion_matrix(true_class_array, predicted_class_array)
print("\nConfusion matrix:\n")
print(confusion_matrix_gnn)

counter = 0
for it, i in zip(predicted_class_array, range(len(predicted_class_array))):
    if it == true_class_array[i]:
        counter += 1

accuracy = counter/len(true_class_array) * 100 
print("Accuracy: {}%".format(accuracy))


########################################################################################################################
# [5.] Explanation =====================================================================================================
########################################################################################################################

graph = dataset[0]
target = graph.y
feats = graph.x
g = nx.Graph()
for edge in graph.edge_index.T:
    g.add_edge(edge[0].item(),edge[1].item())
adj = scipy.sparse.csr_matrix.toarray(nx.adjacency_matrix(g))
lap = scipy.sparse.csr_matrix.toarray(nx.laplacian_matrix(g))
walks = utils.walks(adj)
layout = utils.layout(adj,np.random.randint(0,1000))
ax = plt.subplot()
explain(feats,layout, adj, lap, walks, model, target, gamma = 0.1,ax = ax)
plt.show()