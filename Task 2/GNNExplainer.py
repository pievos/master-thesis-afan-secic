import numpy as np

import networkx as nx
import csv
import dgl
import dgl.data
import torch.nn.functional as F
import torch
from torch_geometric.data import DataLoader
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import copy, time

from features_computation import get_genes, get_genes_bernoulli
from gnn import Classifier, MUTAG_Classifier
from gnn_training_utils import collate
from graph_dataset import GraphDataset


from torch_geometric.datasets import TUDataset
from torch_geometric.data.data import Data


import matplotlib.pyplot as plt
import pickle
from dataset import generate

from gnn_explainer import GNNExplainer

def generate_confusion(xs, ys):
    confusions = []
    for x,y in zip(xs,ys):
        if x == y:
            if x == 1:
                confusions.append("TP") 
            if x == 0:
                confusions.append("TN")
        else:
            if x == 1:
                confusions.append("FP")
            if x == 0:
                confusions.append("FN")
    return confusions

########################################################################################################################
# [1.] Initialize all graphs ::: Structure, features and computed labels ===============================================
########################################################################################################################
graphs_nr = 1000
nodes_per_graph_nr = 10

dataset = generate(graphs_nr,nodes_per_graph_nr) 
'''
# For writing csv file
with open('test.csv', 'w', newline='') as f:
    x = dataset[0].edge_index[0]
    y = dataset[0].edge_index[1]
    for a,b in zip(x,y):
        f.write(f"{a}-{b},")
'''
#torch.save(dataset,'./dataset.bin')
########################################################################################################################
# [2.] GNN training preparation: define the dataset of the graphs and the batch details ================================
########################################################################################################################
               
dataloader = DataLoader(
    dataset,
    batch_size=64,
    shuffle=True)

########################################################################################################################
# [3.] GNN training ====================================================================================================
########################################################################################################################


epoch_nr = 75

input_dim = 1
n_classes = 2
model = MUTAG_Classifier(input_dim, n_classes)
model.train()
opt = torch.optim.Adam(model.parameters(), lr = 0.0001)
for epoch in range(epoch_nr):
    epoch_loss = 0
    graph_idx = 0
    for data in dataloader:
        batch = []
        for i in range(data.y.size(0)):
            for j in range(nodes_per_graph_nr):
                batch.append(i)
        logits = model(data, torch.tensor(batch))
        loss = F.nll_loss(logits, data.y)

        opt.zero_grad()
        loss.backward()
        opt.step()

        epoch_loss += loss.detach().item()
        graph_idx += 1
    epoch_loss /= graph_idx
    print('Epoch {}, loss {:.4f}'.format(epoch, epoch_loss))

########################################################################################################################
# [4.] Prediction ======================================================================================================
########################################################################################################################

confusion_array = []

true_class_array = []
predicted_class_array = []
model.eval()
correct = 0
true_class_array = []
predicted_class_array = []

for data in dataloader:
    batch = []
    for i in range(data.y.size(0)):
        for j in range(nodes_per_graph_nr):
            batch.append(i)

    output = model(data, torch.tensor(batch))

    predicted_class = output.max(dim=1)[1]
    true_class = data.y
    predicted_class_array = np.append(predicted_class_array, predicted_class)
    true_class_array = np.append(true_class_array, true_class)

    correct += predicted_class.eq(data.y).sum().item()

    confusion_array.append(generate_confusion(predicted_class, true_class))

confusion_matrix_gnn = confusion_matrix(true_class_array, predicted_class_array)
print("\nConfusion matrix:\n")
print(confusion_matrix_gnn)

counter = 0
for it, i in zip(predicted_class_array, range(len(predicted_class_array))):
    if it == true_class_array[i]:
        counter += 1

accuracy = counter/len(true_class_array) * 100 
print("Accuracy: {}%".format(accuracy))

########################################################################################################################
# [5.] Explanation =====================================================================================================
########################################################################################################################

#Checkpoint for model and optimizer with good results
checkpoint = {
    'model' : model,
    'optimizer' : opt,
}
checkpoint_path = './checkpoint.pth'
#torch.save(checkpoint,checkpoint_path)

#Loading model and optimizer parameters for running tests
#checkpoint = torch.load(checkpoint_path)
#model = checkpoint['model']
#opt = checkpoint['optimizer']

#Needed for global_add_pool in forward pass
batch = []
for j in range(nodes_per_graph_nr):
    batch.append(0)

graph = dataset[0]
explainer = GNNExplainer(model, epochs=300, lr=0.0001)
nfm, em = explainer.explain_graph(graph,torch.tensor(batch))
explainer.visualize_subgraph(None,graph.edge_index,em)
plt.show()
'''
# For writing csv file
with open('test.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow('\n')

for graph in dataset:
    explain_model = model
    batch = []
    for j in range(nodes_per_graph_nr):
        batch.append(0)
    explainer = GNNExplainer(explain_model, epochs=150)
    nfm, em = explainer.explain_graph(graph,torch.tensor(batch))
    with open('test.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(np.around(em.tolist(), decimals=3))


with open('test.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(confusion_array)

'''