"""
    Explain method for LRP

    :author: Afan Secic (main developer), Anna Saranti (corrections and refactoring)
    :copyright: © 2020 HCI-KDD (ex-AI) group
    :date: 2021-03-03
"""
import numpy as np
import matplotlib.pyplot as plt

def explain(feats, layout, adj, lap, walks, nn, t, gamma=None, ax=None):
    # Arrange graph layout
    r = layout
    r = r - r.min(axis=0)
    r = r / r.max(axis=0) * 2 - 1
    
    # Plot the graph
    N = len(adj)
    for i in np.arange(N):
        for j in np.arange(N):
            if adj[i,j] > 0 and i != j: plt.plot([r[i,0],r[j,0]],[r[i,1],r[j,1]], color='gray', lw=0.5, ls='dotted')
    ax.plot(r[:,0],r[:,1],'o',color='black',ms=3)
    
    for (i,j,k) in walks:
        R = nn.lrp(feats, lap, gamma, t, (j,k))[i].sum()
        tx,ty = utils.shrink([r[i,0],r[j,0],r[k,0]],[r[i,1],r[j,1],r[k,1]])

        if R > 0.0:
            alpha = np.clip(20*R.data.numpy(),0,1)
            ax.plot(tx,ty,alpha=alpha,color='red',lw=1.2) 

        if R < -0.0:
            alpha = np.clip(-20*R.data.numpy(),0,1)
            ax.plot(tx,ty,alpha=alpha,color='blue',lw=1.2)
