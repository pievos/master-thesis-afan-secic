"""
    Graph Neural Network for LRP

    :author: Afan Secic (main developer), Anna Saranti (corrections and refactoring)
    :copyright: © 2020 HCI-KDD (ex-AI) group
    :date: 2021-03-03
"""

import utils
import numpy as np
import scipy
import networkx as nx
import dgl
import dgl.data
import torch.nn.functional as F
import torch
from torch_geometric.data import DataLoader
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import copy, time

from features_computation import get_genes, get_genes_bernoulli
from gnn import Classifier
from gnn_training_utils import collate
from graph_dataset import GraphDataset

from torch_geometric.nn.models import GNNExplainer

from torch_geometric.datasets import TUDataset
from torch_geometric.data.data import Data


import matplotlib.pyplot as plt
import pickle
import torch.nn as nn
from torch_geometric.nn import GCNConv, global_add_pool, GINConv

from torch.nn import Sequential, Linear, ReLU

class Classifier_LRP(torch.nn.Module):
    def __init__(self, input_dim, num_classes):
        super(Classifier_LRP, self).__init__()
        self.conv1 = GCNConv(input_dim, 32)
        self.conv2 = GCNConv(32, 16)
        self.classify = nn.Linear(16,num_classes)

        
    def forward(self, data, batch):
        x, edge_index = data.x, data.edge_index
        
        x = self.conv1(x, edge_index)
        x = F.relu(x)
        x = F.dropout(x, training=self.training)
        x = self.conv2(x, edge_index)
        x = global_add_pool(x, batch)
        x = F.relu(self.classify(x))
        x = F.dropout(x, p=0.5, training=self.training)
        return F.log_softmax(x, dim=-1)

    def lrp(self,feats, A,gamma,l,inds):
        A = torch.tensor(A)
        feats = torch.transpose(feats,0,1)[0]
        if inds is not None:
            j,k = inds
            Mj = torch.FloatTensor(np.eye(len(A))[j][:,np.newaxis])
            Mk = torch.FloatTensor(np.eye(len(A))[k][:,np.newaxis])

        params = self.state_dict()
        c1 = params['conv1.weight'].clone().double()
        conv1p = c1 + gamma*c1.clamp(min=0)
        c2 = params['conv2.weight'].clone().long()
        conv2p = (c2 + gamma*c2.clamp(min=0)).long()
        cl = params['classify.weight'].clone().float()
        cl = torch.transpose(cl,0,1)
        classifyp = cl + gamma*cl.clamp(min=0)

        X = torch.diag(feats) #7x7
        X.requires_grad_(True)

        H  = X.matmul(c1).clamp(min=0)  # Ht = ro(Zt*Wt) 7x32

        H = H.long()
        

        Z  = A.transpose(1,0).matmul(H.matmul(c2)) # Zt = Lamda * Ht-1
        Zp = A.transpose(1,0).matmul(H.matmul(conv2p))
        H  = (Zp*(Z/(Zp+1e-6)).data).clamp(min=0)

        if inds is not None: 
            H = H * Mj + (1-Mj) * (H.data)

        Z  = H.matmul(cl)
        Zp = H.matmul(classifyp)
        H  = (Zp*(Z/(Zp+1e-6)).data).clamp(min=0)

        if inds is not None: 
            H = H * Mk + (1-Mk) * (H.data)

        Y = H.mean(dim=0)[l]
        Y.requires_grad_(True)
        
        Y.backward()

        return X.data*X.grad
