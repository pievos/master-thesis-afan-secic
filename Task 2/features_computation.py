"""
    Computation of the features of the nodes in the graph

    :author: Afan Secic (main developer), Anna Saranti (corrections and refactoring)
    :copyright: © 2020 HCI-KDD (ex-AI) group
    :date: 2020-12-01
"""

from collections import Counter

import dgl
import numpy as np


def sigmoid(x: int):
    """"
    Compute sigmoid function by definition
    """
    return 1/(1+np.exp(x))

def get_genes(graphs_nr: int, nodes_per_graph_nr: int, graph: dgl.DGLGraph):
    """
    Compute the genes/features for all graphs

    :param graphs_nr: Number of graphs
    :param nodes_per_graph_nr: Number of nodes per graph
    :param graph: Graph for computation
    :return:
    """

    # Generate random integer arrays for the features of all nodes -----------------------------------------------------
    randint_gen_upper_non_incl_limit = 2   # Parameter is one above the highest such integer ---------------------------
    genes = np.random.randint(randint_gen_upper_non_incl_limit, size=(graphs_nr, nodes_per_graph_nr))
    
    
    # Get edges of graph -----------------------------------------------------------------------------------------------
    edges = list(graph.edges())
    
    #Select nodes for label calculation --------------------------------------------------------------------------------
    edge_idx = np.random.randint(len(edges))
    node_indices = [edges[edge_idx][0], edges[edge_idx][1]]
    #Print node indices so we know which 2 nodes we want to look in explanation
    print(f"Nodes taken into computatio of the class {str(node_indices)} with edge index {edge_idx}")
    target_labels_all_graphs = []    

    # Compute the target for each patient ------------------------------------------------------------------------------
    for gene in genes:
        # Set the label to XOR of node feature values ------------------------------------------------------------------
        target_labels_all_graphs.append(gene[node_indices[0]] ^ gene[node_indices[1]])

    # Target labels ----------------------------------------------------------------------------------------------------
    print(f"Number of unique classes: {Counter(target_labels_all_graphs).keys()}")
    print(f"Number of labels for each class: {Counter(target_labels_all_graphs).values()}")

    # "target_labels_all_graphs" should just have length (nodes_per_graph_nr,) and not (nodes_per_graph_nr, 5) ---------

    return genes, target_labels_all_graphs


def get_genes_bernoulli(graphs_nr: int, nodes_per_graph_nr: int, graph: dgl.DGLGraph):
    """
    Compute the genes/features for all graphs

    :param graphs_nr: Number of graphs
    :param nodes_per_graph_nr: Number of nodes per graph
    :param graph: Graph for computation
    :return:
    """
    # Generate random number arrays from normal distribution for the features of all nodes -----------------------------------------------------
    mi = 0
    sigma = 1
    genes = np.random.normal(mi, sigma, size=(graphs_nr, nodes_per_graph_nr))

    # Get edges of graph -----------------------------------------------------------------------------------------------
    edges = list(graph.edges())

    #Select nodes for label calculation --------------------------------------------------------------------------------
    edge_idx = np.random.randint(len(edges))
    node_indices = [edges[edge_idx][0], edges[edge_idx][1]]
    
    #Print node indices so we know which 2 nodes we want to look in explanation
    print("Nodes taken into computatio of the class " + str(node_indices))
    
    target_labels_all_graphs = []

    # Compute the target for each patient ------------------------------------------------------------------------------
    for gene in genes:
        # Set the label to a sample from Bernoulli distribution ------
        z = 1 + 2*gene[node_indices[0]] + 3*gene[node_indices[1]]
        prob = sigmoid(z)
        target = np.random.binomial(1,prob)
        target_labels_all_graphs.append(target)

    # Target labels ----------------------------------------------------------------------------------------------------
    print(f"Number of unique classes: {Counter(target_labels_all_graphs).keys()}")
    print(f"Number of labels for each class: {Counter(target_labels_all_graphs).values()}")

    # "target_labels_all_graphs" should just have length (nodes_per_graph_nr,) and not (nodes_per_graph_nr, 5) ---------
    return genes, target_labels_all_graphs