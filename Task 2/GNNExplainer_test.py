import numpy as np

import networkx as nx
import dgl
import dgl.data
import torch.nn.functional as F
import torch
from torch_geometric.data import DataLoader
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import copy, time

from features_computation import get_genes, get_genes_bernoulli
from gnn import Classifier
from gnn_training_utils import collate
from graph_dataset import GraphDataset

from torch_geometric.nn.models import GNNExplainer

from torch_geometric.datasets import TUDataset
from torch_geometric.data.data import Data


import matplotlib.pyplot as plt
import pickle
from dataset import generate

########################################################################################################################
# [1.] Initialize all graphs ::: Structure, features and computed labels ===============================================
########################################################################################################################
graphs_nr = 1000
nodes_per_graph_nr = 7

dataset = generate(graphs_nr,nodes_per_graph_nr)# The dataset is comprised by all those graphs -----------------------------------------------------

########################################################################################################################
# [2.] GNN training preparation: define the dataset of the graphs and the batch details ================================
########################################################################################################################
               
dataloader = DataLoader(
    dataset,
    batch_size=64,
    shuffle=True)

########################################################################################################################
# [3.] GNN training ====================================================================================================
########################################################################################################################


epoch_nr = 350

input_dim = 1
n_classes = 2
model = Classifier(input_dim, n_classes)
model.train()
opt = torch.optim.Adam(model.parameters(), lr = 0.0001)
for epoch in range(epoch_nr):
    epoch_loss = 0
    graph_idx = 0
    for data in dataloader:
        batch = []
        for i in range(data.y.size(0)):
            for j in range(nodes_per_graph_nr):
                batch.append(i)
        logits = model(data, torch.tensor(batch))
        loss = F.nll_loss(logits, data.y)

        opt.zero_grad()
        loss.backward()
        opt.step()

        epoch_loss += loss.detach().item()
        graph_idx += 1
    epoch_loss /= graph_idx
    print('Epoch {}, loss {:.4f}'.format(epoch, epoch_loss))

########################################################################################################################
# [4.] Prediction ======================================================================================================
########################################################################################################################

true_class_array = []
predicted_class_array = []
model.eval()
correct = 0
true_class_array = []
predicted_class_array = []

for data in dataloader:
    batch = []
    for i in range(data.y.size(0)):
        for j in range(nodes_per_graph_nr):
            batch.append(i)

    output = model(data, torch.tensor(batch))

    predicted_class = output.max(dim=1)[1]
    true_class = data.y
    predicted_class_array = np.append(predicted_class_array, predicted_class)
    true_class_array = np.append(true_class_array, true_class)

    correct += predicted_class.eq(data.y).sum().item()

confusion_matrix_gnn = confusion_matrix(true_class_array, predicted_class_array)
print("\nConfusion matrix:\n")
print(confusion_matrix_gnn)

counter = 0
for it, i in zip(predicted_class_array, range(len(predicted_class_array))):
    if it == true_class_array[i]:
        counter += 1

accuracy = counter/len(true_class_array) * 100 
print("Accuracy: {}%".format(accuracy))

#Checkpoint for model and optimizer with good results
checkpoint = {
    'model' : model,
    'optimizer' : opt,
}
checkpoint_path = './checkpoint.pth'
#torch.save(checkpoint,checkpoint_path)

#Loading model and optimizer parameters for running tests
checkpoint = torch.load(checkpoint_path)
model = checkpoint['model']
opt = checkpoint['optimizer']
# nodes 2,6 for saved graphs
#Load one saved graph
with open('graph.bin','rb') as f:
    graph = pickle.load(f)

#Needed for global_add_pool in forward pass
batch = []
for j in range(nodes_per_graph_nr):
    batch.append(0)
    
explainer = GNNExplainer(model, epochs=500)
em, pred = explainer.explain_graph(graph,torch.tensor(batch))
explainer.visualize_subgraph(node_idx=None, edge_index = graph.edge_index, edge_mask = em)
plt.show()
#For generating images for all variations of predicted class and true class
'''
datap0t0, datap0t1, datap1t0, datap1t1 = [], [], [], []

with open('datap0t0.bin','rb') as f:
    datap0t0 = pickle.load(f)

with open('datap0t1.bin','rb') as f:
    datap0t1 = pickle.load(f)

with open('datap1t0.bin','rb') as f:
    datap1t0 = pickle.load(f)

with open('datap1t1.bin','rb') as f:
    datap1t1 = pickle.load(f)

for g,idx in zip(datap0t0, range(len(datap0t0))):
    em, pred = explainer.explain_graph(g,torch.tensor(batch))
    explainer.visualize_subgraph(node_idx=None, edge_index = g.edge_index, edge_mask = em)
    plt.savefig('experiments_2_6/p0t0/Figure{}_p{}_t{}'.format(idx,pred.item(),g.y.item()))
    plt.clf()
for g,idx in zip(datap0t1, range(len(datap0t1))):
    em, pred = explainer.explain_graph(g,torch.tensor(batch))
    explainer.visualize_subgraph(node_idx=None, edge_index = g.edge_index, edge_mask = em)
    plt.savefig('experiments_2_6/p0t1/Figure{}_p{}_t{}'.format(idx,pred.item(),g.y.item()))
    plt.clf()
for g,idx in zip(datap1t0, range(len(datap1t0))):
    em, pred = explainer.explain_graph(g,torch.tensor(batch))
    explainer.visualize_subgraph(node_idx=None, edge_index = g.edge_index, edge_mask = em)
    plt.savefig('experiments_2_6/p1t0/Figure{}_p{}_t{}'.format(idx,pred.item(),g.y.item()))
    plt.clf()
for g,idx in zip(datap1t1, range(len(datap1t1))):
    em, pred = explainer.explain_graph(g,torch.tensor(batch))
    explainer.visualize_subgraph(node_idx=None, edge_index = g.edge_index, edge_mask = em)
    plt.savefig('experiments_2_6/p1t1/Figure{}_p{}_t{}'.format(idx,pred.item(),g.y.item()))
    plt.clf()
'''