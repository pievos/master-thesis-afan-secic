"""
    Generating dataset for GNN

    :author: Afan Secic (main developer), Anna Saranti (corrections and refactoring)
    :copyright: © 2020 HCI-KDD (ex-AI) group
    :date: 2020-12-01
"""

import numpy as np
import scipy
import networkx as nx
import dgl
import dgl.data
import copy, time
import torch
import pickle  

from features_computation import get_genes, get_genes_bernoulli
from graph_dataset import GraphDataset

from torch_geometric.data.data import Data


def generate(graphs_nr, nodes_per_graph_nr):
    graph = nx.generators.random_graphs.barabasi_albert_graph(nodes_per_graph_nr, 1)
    edges = torch.zeros(size=(2,len(graph.edges())), dtype=torch.long)
    for e, idx in zip(graph.edges(), range(len(graph.edges()))):
        edges[0][idx] = e[0]
        edges[1][idx] = e[1]

    genes, target_labels_all_graphs = get_genes(graphs_nr, nodes_per_graph_nr,graph)
    graph = dgl.from_networkx(graph)
    #edges = graph.edges()
    #edges = torch.stack(edges,dim = 0)
    graphs = []
    feats = np.zeros(shape = (graphs_nr, nodes_per_graph_nr))
    for graph_idx in range(graphs_nr):
        temp_graph = copy.deepcopy(graph)
        temp_graph.ndata['feat'] = torch.tensor(np.array(genes[graph_idx]))
        feats[graph_idx] = genes[graph_idx]
        target = target_labels_all_graphs[graph_idx]
        graphs.append((temp_graph, torch.tensor(target)))


    # Generating the dataset in needed form 
    data_graphs = []
    for graph_idx in range(graphs_nr):
        data_graphs.append(Data(x = torch.transpose(torch.Tensor([feats[graph_idx]]),0,1),
                                edge_index = edges, 
                                y = torch.tensor(target_labels_all_graphs[graph_idx], dtype = torch.long)))

    dataset = GraphDataset(data_graphs)
    #create_files(dataset, nodes_per_graph_nr)
    return dataset

def generate_lrp(graphs_nr, nodes_per_graph_nr):
    graph = nx.generators.random_graphs.barabasi_albert_graph(nodes_per_graph_nr, 1)
    genes, target_labels_all_graphs = get_genes_bernoulli(graphs_nr, nodes_per_graph_nr,graph)
    graph = dgl.from_networkx(graph)
    edges = graph.edges()
    edges = torch.stack(edges,dim = 0)
    graphs = []
    feats = np.zeros(shape = (graphs_nr, nodes_per_graph_nr))
    for graph_idx in range(graphs_nr):
        temp_graph = copy.deepcopy(graph)
        temp_graph.ndata['feat'] = torch.tensor(np.array(genes[graph_idx]))
        feats[graph_idx] = genes[graph_idx]
        target = target_labels_all_graphs[graph_idx]
        graphs.append((temp_graph, torch.tensor(target)))

    # Generating the dataset in needed form 
    data_graphs = []
    for graph_idx in range(graphs_nr):
        data_graphs.append(Data(x = torch.diag(torch.tensor(feats[graph_idx])),
                                edge_index = edges, 
                                y = torch.tensor(target_labels_all_graphs[graph_idx], dtype = torch.long)))

    dataset = GraphDataset(data_graphs)
    return dataset

# Used to create files for official implementation of GNNExplainer
def create_files(dataset, nodes_per_graph_nr):
    with open('./master_graph_labels.txt','w') as f:
        for g in dataset:
            f.write(str(g.y.item()) + "\n")
    
    with open('./master_node_attributes.txt','w') as f:
        for g in dataset:
            for item, idx in zip(g.x,range(len(g.x))):
                f.write(str(item.item()))
                f.write("\n")

    with open('./master_graph_indicator.txt', 'w') as f:
        for g,idx in zip(dataset, range(len(dataset))):
            for i in range(len(g.x)):
                f.write(str(idx+1) + "\n")
    
    with open('./master_A.txt', 'w') as f:
        for g,idx in zip(dataset,range(len(dataset))):
            for edge in torch.transpose(g.edge_index,0,1):
                f.write("{},{}\n".format(edge[0]+1+nodes_per_graph_nr*idx,edge[1]+1+nodes_per_graph_nr*idx))

    with open('./master_node_labels.txt','w') as f:
        for g in dataset:
            for idx in range(len(g.x)):
                f.write(str(idx) + "\n")

