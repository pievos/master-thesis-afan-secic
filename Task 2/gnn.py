"""
    Graph Neural Network

    :author: Afan Secic (main developer), Anna Saranti (corrections and refactoring)
    :copyright: © 2020 HCI-KDD (ex-AI) group
    :date: 2020-12-01
"""


import torch
import torch.nn.functional as F
import torch.nn as nn
from torch_geometric.nn import GCNConv, global_add_pool, GINConv

from torch.nn import Sequential, Linear, ReLU


import sklearn.metrics
class MUTAG_Classifier(torch.nn.Module):
    def __init__(self, input_dim, num_classes):
        super(MUTAG_Classifier, self).__init__()
        #Architecture taken from example for MUTAG dataset on graph classification
        dim = 32

        nn1 = Sequential(Linear(input_dim, dim), ReLU(), Linear(dim, dim))
        self.conv1 = GINConv(nn1)
        self.bn1 = torch.nn.BatchNorm1d(dim)

        nn2 = Sequential(Linear(dim, dim), ReLU(), Linear(dim, dim))
        self.conv2 = GINConv(nn2)
        self.bn2 = torch.nn.BatchNorm1d(dim)

        nn3 = Sequential(Linear(dim, dim), ReLU(), Linear(dim, dim))
        self.conv3 = GINConv(nn3)
        self.bn3 = torch.nn.BatchNorm1d(dim)

        nn4 = Sequential(Linear(dim, dim), ReLU(), Linear(dim, dim))
        self.conv4 = GINConv(nn4)
        self.bn4 = torch.nn.BatchNorm1d(dim)

        nn5 = Sequential(Linear(dim, dim), ReLU(), Linear(dim, dim))
        self.conv5 = GINConv(nn5)
        self.bn5 = torch.nn.BatchNorm1d(dim)

        self.fc1 = Linear(dim, dim)
        self.fc2 = Linear(dim, num_classes)

    def forward(self,data,batch):
        x, edge_index = data.x, data.edge_index
        #Forward pass for new architecture
        x = F.relu(self.conv1(x,edge_index))
        x = self.bn1(x)
        x = F.relu(self.conv2(x,edge_index))
        x = self.bn2(x)
        x = F.relu(self.conv3(x,edge_index))
        x = self.bn3(x)
        x = F.relu(self.conv4(x,edge_index))
        x = self.bn4(x)
        x = F.relu(self.conv5(x, edge_index))
        x = self.bn5(x)
        x = global_add_pool(x, batch)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x, dim=-1)


class Classifier(torch.nn.Module):
    def __init__(self, input_dim, num_classes):
        super(Classifier, self).__init__()
        self.conv1 = GCNConv(input_dim, 32)
        self.conv2 = GCNConv(32, 16)
        self.classify = nn.Linear(16,16)
        self.classify2 = nn.Linear(16, num_classes)
        
        
    def forward(self, data, batch):
        x, edge_index = data.x, data.edge_index
        x = self.conv1(x, edge_index)
        x = F.relu(x)
        x = F.dropout(x, training=self.training)
        x = self.conv2(x, edge_index)
        x = global_add_pool(x, batch)
        x = F.relu(self.classify(x))
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.classify2(x)
        return F.log_softmax(x, dim=-1)
        